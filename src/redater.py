#!/usr/local/bin/python2.7

import logging
import os
import re
import configpath
import oggerutils

ISODATE_PATTERN = re.compile ("^\s*(\d+)[-_/\\:\s](\d+)[-_/\\:\s](\d+)\s*$")
YEAR_PATTERN = re.compile ("^\s*(\d{4})\s*$")
AUDIO_EXTENSIONS = [".ogg", ".flac", ".mp3", ".ape"]
TAG_ACQUISITION = "acquisition"
TAG_DATE = "date"

# #
# #
# #


class RedaterConfig(oggerutils.Config) :

    def logConfigOptions (self):
        oggerutils.Config.logConfigOptions(self)
        logging.info("Input directories: {}".format(self.inputDirs))

    def setConfigOptions (self, config):
        oggerutils.Config.setConfigOptions(self, config)
        self.inputDirs = config.get("input_dirs")

    def getInputDirs (self):
        return self.getAsList(self.inputDirs)

# #
# #
# #


def setModificationDate (filePath, dateToSet):
    modificationTime = oggerutils.isoDateToEpoch(dateToSet)
    if os.path.getmtime (filePath) != modificationTime:
        logging.debug("Redating {}: {}".format(filePath, dateToSet))
        oggerutils.setModificationTime (filePath, modificationTime)


def doRedate (itemPath, root):  # @UnusedVariable
    return redate(itemPath)


def redate (itemPath):
    if not oggerutils.getFileExtension (itemPath) in AUDIO_EXTENSIONS:
        return None

    tags = oggerutils.getFileTags(itemPath)
    if tags == None:
        logging.warn("file has no tags: " + itemPath)
        return None

    acquisitionDate = oggerutils.getFirstTagEntry(tags, TAG_ACQUISITION)
    if acquisitionDate != None:
        try:
            setModificationDate (itemPath, acquisitionDate)
            return None
        except ValueError:
            return oggerutils.SkipHash("{}: Invalid acquisition date '{}' (format should be YYYY-MM-DD)".format(itemPath, acquisitionDate))

    normalDate = oggerutils.getFirstTagEntry(tags, TAG_DATE)
    if normalDate == None:
        return oggerutils.SkipHash("{}: no date".format(itemPath))

    try:
        dateToSet = oggerutils.sanitizeDate(normalDate)
        if dateToSet == None:
            matcher = YEAR_PATTERN.match (normalDate)
            if matcher:
                dateToSet = matcher.group(1) + "-01-01"

        if dateToSet == None:
            raise ValueError()

        setModificationDate (itemPath, dateToSet)
        return None
    except ValueError:
        return oggerutils.SkipHash("{}: Invalid date '{}'".format(itemPath, normalDate))

# #
# #
# #


def main ():
    config = RedaterConfig(configpath.REDATER_CONFIG_PATH)

    cachePath = os.path.join(os.path.split(configpath.REDATER_CONFIG_PATH)[0], ".redater_cache")
    oggerutils.hashWalkDirectory (config.getInputDirs(), doRedate, config.disableCache, cachePath)

    logging.info("Done =)")


if __name__ == "__main__":
    main()
