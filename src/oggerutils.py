#!/usr/local/bin/python2.7

from datetime import date
import datetime
import logging
import sys
import os
import pickle
import re
import time
import hashlib

import mutagen
from mutagen.easyid3 import EasyID3
from mutagen.monkeysaudio import MonkeysAudio
from mutagen.mp3 import MP3

CONFIG_PATTERN = re.compile ("(\w+).*=(.*)");
YYYYMMDD_PATTERN = re.compile ("^\s*(\d{4})[-_/\\:\s](\d+)[-_/\\:\s](\d+)\s*$")  # ISO
DDMMYYYY_PATTERN = re.compile ("^\s*(\d+)[-_/\\:\s](\d+)[-_/\\:\s](\d{4})\s*$")


class Config :

    def __init__ (self, configPath):
        config = self.readConfigFile (configPath)
        self.setConfigOptions (config)

        setupLogging (self.logPath if self.shouldDoFileLogging() else None)
        self.logConfigOptions()

    def readConfigFile (self, filePath):
        config = {}
        fh = open (filePath, 'rb')
        for line in fh:
            self.readConfigLine (line.decode(), config)
        fh.close()
        return config

    def readConfigLine (self, line, config):
        matcher = CONFIG_PATTERN.match (line)
        if not matcher:
            return

        key = matcher.group(1).strip()
        values = filter(None, re.split("[,;]", matcher.group(2)))

        sanitizedValues = []
        for value in values:
            sanitizedValues.append(value.strip())

        if len(sanitizedValues) == 1:
            config[key] = sanitizedValues[0]
        else :
            config[key] = sanitizedValues

    def logConfigOptions (self):
        logging.info("Disable cache: {}".format(self.disableCache))
        logging.info("Log path: {}".format(self.logPath))

    def setConfigOptions (self, config):
        self.disableCache = valueIsPositive(config.get("disable_cache"))
        self.logPath = config.get("log_path")

    def shouldDoFileLogging (self):
        return self.logPath != None

    def getAsList(self, value):
        return value if type(value) is list else [value]


def setupLogging (fileLogPath):
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logging.Formatter("[%(levelname)-5.5s] %(message)s"))
    consoleHandler.setLevel(logging.INFO)
    rootLogger.addHandler(consoleHandler)

    if fileLogPath != None :
        fileHandler = logging.FileHandler(fileLogPath)
        logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        fileHandler.setFormatter(logFormatter)
        fileHandler.setLevel(logging.DEBUG)
        rootLogger.addHandler(fileHandler)
# #
# #
# #


def directoryHashesFromFile (filePath):
    dirHashes = DirectoryHashes()
    dirHashes.loadResultsFromFile(filePath)
    return dirHashes


class SkipHash :

    def __init__ (self, message):
        self.message = message


class DirectoryHashes :

    def __init__ (self):
        self.hashes = {}

    def loadResultsFromFile (self, filePath):
        fileObject = open(filePath, 'rb')
        self.hashes = pickle.load(fileObject, encoding="bytes")
        fileObject.close()

    def saveResultsToFile (self, filePath):
        fileObject = open(filePath, 'wb')
        pickle.dump(self.hashes, fileObject)
        fileObject.close()

    def computeKey(self, dirHash, directoryPath):
        return str(dirHash.hexdigest()) + ":" + directoryPath

    def getResults (self, dirHash, dirPath):
        return self.hashes.get(self.computeKey(dirHash, dirPath))

    def getAllResults (self):
        results = []
        for result in self.hashes.values() :
            results += result

        return results

    def setResults (self, dirHash, dirPath, dirResults):
        self.hashes[self.computeKey(dirHash, dirPath)] = dirResults


def hashWalkDirectory (roots, callback, disableCache, cachePath):
    cachePathExists = os.path.exists(cachePath)

    oldHashes = DirectoryHashes() if disableCache or not cachePathExists else directoryHashesFromFile(cachePath)
    newHashes = DirectoryHashes()

    for root in roots:
        hashWalkDirectory_recursive (root, root, callback, oldHashes, newHashes)

    if not disableCache:
        newHashes.saveResultsToFile (cachePath)

    return newHashes.getAllResults()


def hashWalkDirectory_recursive (root, dirPath, callback, oldHashes, newHashes) :
    dirHash = hashlib.md5()

    for item in os.listdir(dirPath):
        itemString = item
        itemPath = os.path.join (dirPath, itemString)
        dirHash.update (itemString.encode())
        dirHash.update (str(os.path.getmtime(itemPath)).encode())
        dirHash.update (str(os.path.getsize(itemPath)).encode())

    localResults = []
    oldResults = oldHashes.getResults(dirHash, dirPath)
    if (oldResults != None):
        localResults = oldResults
        localResultsDone = True
    else :
        localResultsDone = False

    storeResults = True
    for item in os.listdir(dirPath):
        itemPath = os.path.join (dirPath, item)
        if os.path.isfile(itemPath):
            if localResultsDone:
                continue

            result = callback (itemPath, root)
            if isinstance(result, SkipHash):
                storeResults = False
                logging.warn(result.message)
            elif result != None:
                localResults.append(result)

        else :
            hashWalkDirectory_recursive(root, itemPath, callback, oldHashes, newHashes)

    if storeResults:
        newHashes.setResults(dirHash, dirPath, localResults)

# #
# #
# #


def dateToTuple (dateString):
    if dateString == None:
        return None

    matcher = YYYYMMDD_PATTERN.match (dateString)
    if matcher:
        year = int(matcher.group(1))
        month = int(matcher.group(2))
        day = int (matcher.group(3))
        return [year, month, day]

    matcher = DDMMYYYY_PATTERN.match (dateString)
    if matcher:
        day = int (matcher.group(1))
        month = int(matcher.group(2))
        year = int(matcher.group(3))
        return [year, month, day]


def sanitizeDate(dateString):
    dateTuple = dateToTuple (dateString)
    if dateTuple == None:
        return None
    return date(dateTuple[0], dateTuple[1], dateTuple[2]).isoformat()


def isoDateToEpoch (isoDateString):
    if isoDateString == None:
        return None

    dateTuple = dateToTuple (isoDateString)
    date = datetime.datetime(dateTuple[0], dateTuple[1], dateTuple[2], 0, 0, 0)
    return time.mktime(date.timetuple())


def setModificationTime(filePath, modificationTime):
    os.utime(filePath, (modificationTime, modificationTime))

# #
# #
# #


EasyMp3Tags = ['album', 'artist', 'title', 'genre', 'composer', 'date', 'tracknumber', 'discnumber']
ApeTags = {'tracknumber': 'Track', 'date': 'Year'}


def getMP3Tag(tags, key):
    if key in EasyMp3Tags:
        return EasyID3 (tags.filename).get(key)

    mp3Tag = tags.get("TXXX:" + key)
    if mp3Tag == None:
        return None

    return mp3Tag.text


def getApeTag(tags, key):
    if key in ApeTags:
        entries = tags.get(ApeTags[key])
    else :
        entries = tags.get(key.capitalize())

    if entries == None:
        return None

    return entries.value


def getFirstTagEntry (tags, key):
    if type(tags) is MP3:
        entries = getMP3Tag (tags, key)
    elif type(tags) is MonkeysAudio:
        entries = getApeTag (tags, key)
    else :
        entries = tags.get(key)

    if entries == None:
        return None

    if isinstance(entries, list) :
        entry = entries[0]
    else :
        entry = entries

    return entry


def getFileExtension (filePath) :
    return os.path.splitext(filePath)[-1].lower()


def getFileTags (absPath):
    return mutagen.File(absPath)  # @UndefinedVariable


def valueIsPositive (string):
    if string == None:
        return False

    lcString = (str(string)).lower()
    return lcString == "true" or lcString == "yes"
