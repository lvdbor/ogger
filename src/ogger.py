#!/usr/local/bin/python3.8

import datetime
import logging
from multiprocessing.pool import ThreadPool
import os
import re
import shutil
import subprocess
import sys
import time
import stat

import mutagen
from mutagen.oggvorbis import OggVorbis

import configpath
import oggerutils
import redater

ISODATE_PATTERN = re.compile ("^\s*(\d+)[-_/\\:\s](\d+)[-_/\\:\s](\d+)\s*$")
IMAGE_EXTENSIONS = {".jpg", ".png"}
OGG_EXTENSION = ".ogg"
FLAC_EXTENSION = ".flac"
COMPRESSED_EXTENSIONS = [OGG_EXTENSION, ".mp3"]
AUDIO_EXTENSIONS = [FLAC_EXTENSION, OGG_EXTENSION, ".mp3"]
TAG_OGGER_SKIP = "ogger_skip"
TAG_OGGER_STICKY = "ogger_sticky"
TAG_BANNED = "banned"
TAG_ACQUISITION = "acquisition"
TAG_OGGER_ALBUM = "ogger_album"
TAG_OGGER_TRACKNUMBER = "ogger_tracknumber"
TAG_OGGER_IS_MIXTAPE = "ogger_is_mixtape"

# #
# #
# #


class OggerConfig (oggerutils.Config):

	def logConfigOptions (self):
		oggerutils.Config.logConfigOptions(self)
		logging.info("Output directory: {}".format(self.outputDir))
		logging.info("Input directories: {}".format(self.inputDirs))
		logging.info("oggenc path: {}".format(self.oggencPath))
		logging.info("Track limit: {}".format(self.trackLimit))
		logging.info("Recent track limit: {}".format(self.recentTrackLimit))
		logging.info("Pool size: {}".format(self.poolSize))

	def setConfigOptions (self, config):
		oggerutils.Config.setConfigOptions(self, config)
		self.outputDir = config["output_dir"]
		self.inputDirs = config["input_dirs"]
		self.oggencPath = config["oggenc_path"]

		if "track_limit" in config:
			self.trackLimit = int(config["track_limit"])
		else :
			self.trackLimit = 0

		if "recent_track_limit" in config:
			self.recentTrackLimit = int(config["recent_track_limit"])
		else :
			self.recentTrackLimit = 0

		if "pool_size" in config:
			self.poolSize = max(1, int(config["pool_size"]))
		else :
			self.poolSize = 1

	def getInputDirs (self):
		return self.getAsList(self.inputDirs)


class FileInfo :

	def __init__(self, absPath, rootPath) :
		self.absPath = absPath
		self.rootPath = rootPath
		self.relPath = os.path.relpath(absPath, rootPath)
		self.modifiedDate = os.path.getmtime(absPath)

	def __str__ (self) :
		return self.absPath

	__repr__ = __str__


def tagValueIsPositive (tags, key):
	return valueIsPositive(oggerutils.getFirstTagEntry(tags, key))


class InputFileInfo (FileInfo) :

	def __init__(self, absPath, rootPath) :
		FileInfo.__init__(self, absPath, rootPath)

		tags = getFileTags(absPath)
		acquiredDate = oggerutils.getFirstTagEntry(tags, TAG_ACQUISITION)

		try:
			self.acquiredDate = oggerutils.sanitizeDate(acquiredDate)
		except ValueError:
			logging.warn("Invalid acquisition date '{}' (format should be YYYY-MM-DD) for '{}'".format(acquiredDate, absPath))
			self.acquiredDate = None

		self.album = tags.get(TAG_OGGER_ALBUM)
		self.trackNumber = tags.get(TAG_OGGER_TRACKNUMBER)
		self.isMixtape = tagValueIsPositive(tags, TAG_OGGER_IS_MIXTAPE)
		self.sticky = tagValueIsPositive(tags , TAG_OGGER_STICKY)

	def __str__ (self) :
		return self.absPath + ", " + str(self.acquiredDate) + ", " + str(self.sticky)

	def outputFilePath (self, outputDir) :
		inputExtension = getFileExtension(self.absPath)
		if inputExtension in COMPRESSED_EXTENSIONS:
			return os.path.join(outputDir, self.relPath)
		else :
			return os.path.join(outputDir, os.path.splitext(self.relPath)[0] + OGG_EXTENSION)

	def comparator(a, b):  # @NoSelf
		if a.sticky != b.sticky :
			return -1 if a.sticky else 1

		if a.acquiredDate != b.acquiredDate :
			if a.acquiredDate == None:
				return 1
			if b.acquiredDate == None:
				return -1
			if a.acquiredDate > b.acquiredDate:
				return -1
			else :
				return 1

		if (a.modifiedDate > b.modifiedDate):
			return -1
		else :
			return 1

	def sortingInverseKey(a):
		val1 = 1 if a.sticky else 0 
		val2 = "0000-00-00" if a.acquiredDate == None else a.acquiredDate
		val3 = a.modifiedDate
		return (val1, val2, val3)

def sortInputFileInfosByPriority (inputFileInfos):
	return sorted(inputFileInfos, key = InputFileInfo.sortingInverseKey, reverse = True)


def sortInputFileInfosByPath (inputFileInfos):
	return sorted(inputFileInfos, key = lambda info: info.absPath)


class OutputFileInfo (FileInfo) :
	def __init__ (self, absPath, rootPath) :
		FileInfo.__init__ (self, absPath, rootPath)

def getFileSize (path):
	return os.path.getsize (path)


def fileHasTags (path):
	return oggerutils.getFileTags(path) != None

def setFilePermissions (path):
	os.chmod(path, 0o775)

class OggTask:
	def __init__ (self, inputPath, outputPath, inputFileInfo):
		self.inputPath = inputPath
		self.outputPath = outputPath
		self.inputFileInfo = inputFileInfo

	def perform (self):
		global oggTasksDone
		oggTasksDone += 1

		if os.path.exists(self.outputPath):
			logging.warn("Output file already exist, skipping ({} -> {})".format(self.inputPath, self.outputPath))
			return

		if getFileExtension(self.inputPath) in COMPRESSED_EXTENSIONS:
			logging.debug("Output is compressed. Copying: [{}/{}]: {}".format(oggTasksDone, oggTasksToDo, self.inputPath))
			shutil.copyfile (self.inputPath, self.outputPath)
			redater.redate(self.outputPath)
			return

		logging.debug("Ogging: [{}/{}]: {}".format(oggTasksDone, oggTasksToDo, self.inputPath))

		tempOutputFile = self.outputPath + ".part"
		result = subprocess.call([config.oggencPath, "-Q", "-o", tempOutputFile, self.inputPath])
		if result == 0 and getFileSize(tempOutputFile) > 0 and fileHasTags(tempOutputFile):
			redater.redate(tempOutputFile)

			if self.inputFileInfo.album != None:
				outputFile = OggVorbis(tempOutputFile)

				logging.debug ("{}: Override Album tag to '{}'".format(self.outputPath, self.inputFileInfo.album))
				outputFile.tags["original_album"] = outputFile.tags["album"]
				outputFile.tags["album"] = self.inputFileInfo.album
				if "albumartist" in outputFile.tags :
					del (outputFile.tags["albumartist"])

				if self.inputFileInfo.isMixtape or self.inputFileInfo.trackNumber != None :
					if "tracknumber" in outputFile.tags:	
						outputFile.tags["original_tracknumber"] = outputFile.tags["tracknumber"]

					logging.info ("{}: Removing album gain".format (self.outputPath))
					if "replaygain_album_gain" in outputFile.tags:
						del(outputFile.tags["replaygain_album_gain"])
					if "replaygain_album_peak" in outputFile.tags:
						del(outputFile.tags["replaygain_album_peak"])

					if self.inputFileInfo.isMixtape :
						logging.info ("{}: Removing track number".format (self.outputPath))
						if "tracknumber" in outputFile.tags:
							del(outputFile.tags["tracknumber"])
					else :
						logging.debug ("{}: Override tracknumber tag to '{}'".format(self.outputPath, self.inputFileInfo.trackNumber))
						outputFile.tags["tracknumber"] = self.inputFileInfo.trackNumber

				outputFile.save()

			setFilePermissions (tempOutputFile)
			os.rename (tempOutputFile, self.outputPath)
		else :
			logging.error("Something went wrong while ogging {}".format(self.inputPath))
			os.remove(tempOutputFile)

copyImageTasksDone = 0
copyImageTasksToDo = 0

class CopyImagesTask:
	def __init__(self, inputDirPath, outputDirPath):
		self.inputDirPath = inputDirPath
		self.outputDirPath = outputDirPath

	def perform (self):
		global copyImageTasksDone
		copyImageTasksDone += 1

		logging.debug("Copy images in directory [{}/{}]".format(copyImageTasksDone, copyImageTasksToDo))

		for item in os.listdir(self.inputDirPath):
			itemPath = os.path.join(self.inputDirPath, item)
			if not os.path.isfile (itemPath) or getFileExtension(itemPath) not in IMAGE_EXTENSIONS :
				continue

			outputItemPath = os.path.join (self.outputDirPath, item)
			if os.path.exists (outputItemPath) :
				logging.debug("Image {} exists, skipping".format(outputItemPath))
				continue

			outputItemPath = os.path.join (self.outputDirPath, item)
			logging.debug("Copying image: {}".format(itemPath))
			shutil.copy (itemPath, outputItemPath)
			setFilePermissions (outputItemPath)

# #
# #
# #

def getFileExtension (filePath) :
	return os.path.splitext(filePath)[-1].lower()


def getFileTags (absPath):
	return mutagen.File(absPath)  # @UndefinedVariable

def valueIsPositive (string):
	if string == None:
		return False

	lcString = (str(string)).lower()
	return lcString == "true" or lcString == "yes"

# #
# #
# #

def shouldSkipFile (absPath) :
	tags = getFileTags(absPath)
	return tagValueIsPositive(tags, TAG_BANNED) or tagValueIsPositive(tags, TAG_OGGER_SKIP)

def createInputFileInfo(absPath, rootPath):
	if shouldSkipFile(absPath):
		return None

	return InputFileInfo (absPath, rootPath)

# #
# #
# #

def hashWalkCallback (itemPath, root):
	if getFileExtension(itemPath) not in AUDIO_EXTENSIONS:
		return None

	global absPathsToRedate
	absPathsToRedate.append(itemPath)

	return createInputFileInfo(itemPath, root)

# #
# #
# #

def makeSureOutputDirExists (outputDir) :
	if os.path.isdir (outputDir) :
		logging.info("Using output directory: " + outputDir)
	else :
		os.makedirs (outputDir, 0o775)
		logging.info("Created output directory: " + outputDir)
	return os.path.isdir (outputDir)

# #
# #
# #

def scanOutputFiles (outputPath) :
	outputFiles = {}
	for dirPath, _, files in os.walk(outputPath):
		for fileName in files:
			absFilePath = os.path.join(dirPath, fileName)
			if getFileExtension(absFilePath) in COMPRESSED_EXTENSIONS :
				outputFiles[absFilePath] = (OutputFileInfo(absFilePath, outputPath))
	return outputFiles

# #
# #
# #

def processInputFiles (inputFileInfos) :
	outputFileDict = scanOutputFiles (config.outputDir)

	oggTasks = []
	copyImageTasks = {}
	inputFilesInCollection = set()

	stickyTracks = 0
	recentTracks = 0

	for info in inputFileInfos :
		if config.trackLimit > 0 and len(inputFilesInCollection) >= config.trackLimit :
			logging.debug("Track limit reached {} / {}".format(len(inputFilesInCollection), config.trackLimit))
			break

		if config.recentTrackLimit > 0 and not info.sticky and recentTracks >= config.recentTrackLimit :
			logging.debug("Recent track limit reached {} / {}".format(recentTracks, config.recentTrackLimit))
			break

		if info.absPath in inputFilesInCollection:
			continue

		inputFilesToDo = []
		if info.sticky :
			inputFilesToDo = [info]
			stickyTracks += 1
		else :
			inputFilesToDo = getSiblingFiles(info, inputFilesInCollection)
			recentTracks += len(inputFilesToDo)

		for fileToDo in inputFilesToDo :
			inputFilesInCollection.add(fileToDo.absPath)

			outputFile = fileToDo.outputFilePath(config.outputDir)
			if processInputFile (fileToDo, outputFile, outputFileDict):
				oggTasks.append (OggTask(fileToDo.absPath, outputFile, info))

			if outputFile in outputFileDict:
				del outputFileDict[outputFile]

		inputDirPath = os.path.split (info.absPath)[0]
		outputDirPath = os.path.split (info.outputFilePath(config.outputDir))[0]

		if not os.path.exists(outputDirPath) :
			os.makedirs(outputDirPath)

		if inputDirPath not in copyImageTasks :
			copyImageTasks[inputDirPath] = CopyImagesTask(inputDirPath, outputDirPath)

	# remove remaining (obsolete) output files
	removeOutputFiles (sorted(outputFileDict.values(), key = lambda item: item.absPath))

	runTasks(copyImageTasks.values(), oggTasks)


def redateInputFiles(absPaths):
	for absPath in absPaths:
		redater.redate(absPath)

# #
# #
# #


def performOggTask (task):
	task.perform()


def performCopyImagesTask (task):
	task.perform()


def runTasks(copyImageTasks, oggTasks):
	if config.poolSize == 1:
		for task in copyImageTasks:
			task.perform()

		for task in oggTasks:
			task.perform()

		return

	threadPool = ThreadPool(config.poolSize)

	# copy images
	global copyImageTasksToDo
	copyImageTasksToDo = len (copyImageTasks)

	if copyImageTasksToDo > 0:
		threadPool.map (performCopyImagesTask, copyImageTasks)

	# ogg files
	global oggTasksToDo
	oggTasks.sort(key = lambda task: task.inputPath)
	oggTasksToDo = len (oggTasks)

	if oggTasksToDo > 0:
		threadPool.map (performOggTask, oggTasks)

	threadPool.close()
	threadPool.join()


def getSiblingFiles (inputFileInfo, inputFilesInCollection):
	directoryPath = os.path.split (inputFileInfo.absPath)[0]

	siblingFiles = []

	for item in os.listdir(directoryPath):
		itemPath = os.path.join(directoryPath, item)
		if not os.path.isfile (itemPath) or getFileExtension(item) not in AUDIO_EXTENSIONS:
			continue

		if itemPath in inputFilesInCollection:
			continue

		if shouldSkipFile(itemPath) :
			continue

		info = createInputFileInfo (itemPath, inputFileInfo.rootPath)
		if info == None :
			continue

		siblingFiles.append(info)

	return siblingFiles


def processInputFile (inputFileInfo, outputFile, outputFileDict):
	if outputFile in outputFileDict:
		outputFileInfo = outputFileDict[outputFile]

		if inputFileInfo.modifiedDate <= outputFileInfo.modifiedDate :
			logging.debug(inputFileInfo.relPath + ": output exists, skipping")
			return False

		logging.debug("input: " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(inputFileInfo.modifiedDate)))
		logging.debug("output: " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(outputFileInfo.modifiedDate)))
		logging.debug(inputFileInfo.relPath + ": output is older, removing output file, re-ogging")
		os.remove (outputFileInfo.absPath);
	else :
		logging.debug(inputFileInfo.relPath + ": output does not exist, ogging")

	return True


def removeOutputFiles (outputFileDict):
	for outputFile in outputFileDict :
		logging.debug("removing output file: " + str(outputFile))
		os.remove(outputFile.absPath)

# #
# #
# #

def cleanupDir (dirPath):
	dirContents = os.listdir(dirPath)
	if len (dirContents) == 0 :
		return True

	for item in dirContents :
		itemPath = os.path.join(dirPath, item)
		if os.path.isdir(itemPath) :
			result = cleanupDir (itemPath)
			if result :
				os.rmdir(itemPath)

	dirContents = os.listdir(dirPath)
	if len (dirContents) == 0 :
		return True

	for item in dirContents :
		itemPath = os.path.join(dirPath, item)
		if os.path.isdir(itemPath) :
			return False

		if getFileExtension(itemPath) in COMPRESSED_EXTENSIONS :
			return False

	# directory is not empty, and contains no directories, and no OGG_EXTENSION's. Remove what's in there
	for item in dirContents :
		itemPath = os.path.join(dirPath, item)
		os.remove(itemPath)

	return True

# #
# #
# #

def printProspectedOutputFiles (prioritizedInputFiles):
	logging.debug("Prospected output files")

	numberOfInputFiles = len(prioritizedInputFiles)

	i = 0
	while (i < numberOfInputFiles and prioritizedInputFiles[i].sticky and (config.trackLimit == 0 or i < config.trackLimit)) :
		logging.debug(str(prioritizedInputFiles[i]))
		i += 1

	logging.debug ("{} sticky tracks".format(i))

	j = 0
	while (i < numberOfInputFiles and (config.trackLimit == 0 or i < config.trackLimit) and (config.recentTrackLimit == 0 or j < config.recentTrackLimit)) :
		logging.debug(str(prioritizedInputFiles[i]))
		i += 1
		j += 1

	logging.debug ("{} recent tracks".format(j))

# #
# #
# #

oggTasksDone = 0
oggTasksToDo = 0
config = OggerConfig(configpath.OGGER_CONFIG_PATH)

absPathsToRedate = []

def main ():
	startTime = datetime.datetime.now()

	if not os.path.exists(config.oggencPath):
		logging.error ("Can't find oggEnc. Is vorbis-tools installed?")
		exit()

	if not makeSureOutputDirExists (config.outputDir):
		sys.exit ("cannot create output directory: " + config.outputDir)

	# #
	# #
	# #

	cachePath = os.path.join(os.path.split(configpath.REDATER_CONFIG_PATH)[0], ".ogger_cache")
	inputFiles = oggerutils.hashWalkDirectory (config.getInputDirs(), hashWalkCallback, config.disableCache, cachePath)

	prioritizedInputFiles = sortInputFileInfosByPriority (inputFiles)
	printProspectedOutputFiles (prioritizedInputFiles)

	scanTime = datetime.datetime.now()
	logging.debug("scan time: " + str(scanTime - startTime))

	processInputFiles(prioritizedInputFiles)
	processTime = datetime.datetime.now()
	logging.debug("process time: " + str(processTime - scanTime))

	logging.debug("Clean up")
	cleanupDir (config.outputDir)

	logging.debug ("Redating input files")
	redateInputFiles (absPathsToRedate)

	logging.debug ("total time: " + str(datetime.datetime.now() - startTime))

	logging.info("done =)")

if __name__ == "__main__":
	main()
